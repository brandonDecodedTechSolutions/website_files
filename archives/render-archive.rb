#!/usr/bin/env ruby
#
# gem install liquid json
#

require 'liquid'
require 'json'

template_file = File.read("template.html")
years = Dir.entries(".").reject{ |entry| entry =~ /^\.{1,2}$/ }
data = { "years" => [] }
shouldFail = false

years.sort.reverse.each { |year|
   if File.directory?("./#{year}")
       months = Dir.glob("./#{year}/**/*.json")
       yearData = { "name" => year }
       yearData["months"] = [];
       months.sort.reverse.each { |month|
           if File.file?(month)
               monthFile = File.read(month)
               begin
                   monthData = JSON.parse(monthFile)
               rescue JSON::ParserError => e
                    p "JSON Parse Failed for year: #{year} | month: #{month}"
                    shouldFail = true
               end
               yearData["months"] << monthData
           end
       }
      data["years"] << yearData
   end
}

if shouldFail
    p "One or more JSON Parse operations failed.  Exiting without rendering."
    exit(1)
end

template = Liquid::Template.parse(template_file)
output = template.render(data)

File.write("index.html", output)
